//
//  RandomPlaces.h
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/4/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSMutableArray *places = nil;
static NSMutableDictionary *sectionsAndRows = nil;
static NSMutableArray *primoPlaces = nil;
static NSMutableDictionary *primoSectionsAndRows = nil;
static BOOL finishedLoadingPlaces = false;
static NSMutableDictionary *photos = nil;
static NSMutableDictionary *primoPhotos = nil;



@interface RandomPlaces : NSObject

#define MAX_PLACES @25
#define MAX_PHOTOS @15

+(NSMutableArray *)getPlaces;
+(void)setPlaces:(NSDictionary*)dict;
+(int)getSize;
+(void)parseOutSectionsAndRows;
+(NSDictionary *)getSectionsAndRows;
+(NSMutableDictionary*)getPhotos;

+(BOOL)getFinished;
+(void)reloadPlaces;

//-(NSMutableArray *)get100Places;
-(void)getPoundDefineNumberOfPlaces;
-(void)getPlaceInfo:(id)place_id dictionary:(NSDictionary *)json;
@end
