//
//  ViewController.m
//  GeoFlick
//
//  Created by Jeremy Thompson on 3/3/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//
//  some of the zooming techniques came from here :
//  http://www.raywenderlich.com/10518/how-to-use-uiscrollview-to-scroll-and-zoom-content
//

#import "ViewController.h"
#import "FlickrFetcher.h"
#import "RandomPlaces.h"

@interface ViewController () <UIScrollViewDelegate>
@property (nonatomic) UIImageView *iv;
@property (nonatomic, strong) UIImage *image;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (atomic) BOOL waitForResponse;
@property (nonatomic) BOOL zoomedIn;
@end

@implementation ViewController


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self setImageURL:self.imageURL];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = false;
    self.zoomedIn = false;
    self.titleTextView.text = self.textTitle;
    self.descriptionTextView.text = self.textDescription;
    [self.scrollView addSubview:self.iv];
    
    UITapGestureRecognizer *doubleTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewDoubleTapped:)];
    doubleTapRecognizer.numberOfTapsRequired = 2;
    doubleTapRecognizer.numberOfTouchesRequired = 1;
    [self.scrollView addGestureRecognizer:doubleTapRecognizer];
    
    UITapGestureRecognizer *twoFingerTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTwoFingerTapped:)];
    twoFingerTapRecognizer.numberOfTapsRequired = 1;
    twoFingerTapRecognizer.numberOfTouchesRequired = 2;
    [self.scrollView addGestureRecognizer:twoFingerTapRecognizer];
    
    
    
}

- (void)scrollViewTwoFingerTapped:(UITapGestureRecognizer*)recognizer {
    // Zoom out slightly, capping at the minimum zoom scale specified by the scroll view
    CGFloat newZoomScale = self.scrollView.zoomScale / 1.5f;
    newZoomScale = MAX(newZoomScale, self.scrollView.minimumZoomScale);
    [self.scrollView setZoomScale:newZoomScale animated:YES];
}


- (void)scrollViewDoubleTapped:(UITapGestureRecognizer*)recognizer {
    // 1
    CGPoint pointInView = [recognizer locationInView:self.iv];
    
    // 2
    CGFloat newZoomScale = self.scrollView.zoomScale * 1.5f;
    newZoomScale = MIN(newZoomScale, self.scrollView.maximumZoomScale);
    if (!self.zoomedIn) {
        newZoomScale = self.scrollView.maximumZoomScale; self.zoomedIn = true;
    }else{
        newZoomScale = self.scrollView.minimumZoomScale; self.zoomedIn = false;
    }
    
    
    // 3
    CGSize scrollViewSize = self.scrollView.bounds.size;
    
    CGFloat w = scrollViewSize.width / newZoomScale;
    CGFloat h = scrollViewSize.height / newZoomScale;
    CGFloat x = pointInView.x - (w / 2.0f);
    CGFloat y = pointInView.y - (h / 2.0f);
    
    CGRect rectToZoomTo = CGRectMake(x, y, w, h);
    
    // 4
    [self.scrollView zoomToRect:rectToZoomTo animated:YES];
}


-(void)scrollViewDidZoom:(UIScrollView *)scrollView{
    UIImageView *imageView = [scrollView.subviews objectAtIndex:0];
    CGFloat offsetX = MAX((scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5, 0.0);
    CGFloat offsetY = MAX((scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5, 0.0);
    
    imageView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

-(void)doTheThing{
    CGRect scrollViewFrame = self.scrollView.frame;
    CGFloat scaleWidth = scrollViewFrame.size.width / self.scrollView.contentSize.width;
    CGFloat scaleHeight = scrollViewFrame.size.height / self.scrollView.contentSize.height;
    CGFloat minScale = MIN(scaleWidth, scaleHeight);
    self.scrollView.minimumZoomScale = minScale;
    
    self.scrollView.maximumZoomScale = 2.0;
    self.scrollView.zoomScale = minScale;
    [self centerScrollViewContents];
}

- (void)centerScrollViewContents {
    CGSize boundsSize = self.scrollView.bounds.size;
    CGRect contentsFrame = self.iv.frame;
    
    if (contentsFrame.size.width < boundsSize.width) {
        contentsFrame.origin.x = (boundsSize.width - contentsFrame.size.width) / 2.0f;
    } else {
        self.scrollView.contentSize = self.iv.frame.size;
        contentsFrame.origin.x = 0.0f;
    }
    
    if (contentsFrame.size.height < boundsSize.height) {
        contentsFrame.origin.y = (boundsSize.height - contentsFrame.size.height) / 2.0f;
    } else {
        self.scrollView.contentSize = self.iv.frame.size;
        contentsFrame.origin.y = 0.0f;
    }
    
    self.iv.frame = contentsFrame;
}

-(UIView *)viewForZoomingInScrollView: (UIScrollView *)sender{
    return self.iv;
}

-(void)setImageURL:(NSURL *)imageURL{
    _imageURL = imageURL;
    [self startDownloadingImage];
}

-(void)startDownloadingImage{
    self.image = nil;
    
    if(self.imageURL){
        NSURLRequest *request = [NSURLRequest requestWithURL:self.imageURL];
        NSURLSessionConfiguration *conf =[NSURLSessionConfiguration ephemeralSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
        NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request completionHandler:^(NSURL *localFile, NSURLResponse *response, NSError *error) {
            if(!error){
                if([request.URL isEqual:self.imageURL]){
                    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:localFile]];
                    dispatch_async(dispatch_get_main_queue(), ^{ self.image = image;
                        [self.spinner stopAnimating];
                    });
                }
            }
        }];
        [task resume];
    }
}

-(void)setScrollView:(UIScrollView *)scrollView{
    _scrollView = scrollView;
    _scrollView.delegate = self;
    self.scrollView.contentSize = self.image ? self.image.size: CGSizeZero;
    [self doTheThing];
}

-(UIImageView *)iv{
    if (!_iv) _iv = [[UIImageView alloc] init];
    return _iv;
}

-(UIImage *)image{ return self.iv.image; }

-(void)setImage:(UIImage *)image{
    self.iv.image = image;
    [self.iv sizeToFit];
    self.scrollView.contentSize = self.image ? self.image.size: CGSizeZero;
    [self doTheThing];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
