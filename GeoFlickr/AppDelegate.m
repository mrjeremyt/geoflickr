//
//  AppDelegate.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/3/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "AppDelegate.h"
#import "RecentPhotos.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *array = [RecentPhotos getRecentPhotos];
    NSMutableArray *result = [[NSMutableArray alloc] init];
    for (int i = 0; i < [array count]; i++) {
        NSMutableDictionary *dict = array[i];
        NSString *title = dict[@"title"];
        NSString *description = dict[@"description"];
        NSString *imageUrl = [dict[@"imageURL"] absoluteString];
        [dict removeAllObjects];
        [dict setObject:title forKey:@"title"];
        [dict setObject:description forKey:@"description"];
        [dict setObject:imageUrl forKey:@"imageURL"];
        [result addObject:dict];
    }
//    NSArray *t = result;
    [defaults setObject:result forKey:@"recents"];
    [defaults synchronize];
}

@end
