//
//  RandomPlaces.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/4/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "RandomPlaces.h"
#import "FlickrFetcher.h"

@interface RandomPlaces()

@property (atomic) BOOL placeDetailsDone;
@property (atomic) NSNumber *counter;
@property (nonatomic) int howMany;
@end

@implementation RandomPlaces

+(int)getSize{
    return (int)[places count];
}

+(NSMutableArray *)getPlaces{
    return places;
}

+(void)setPlaces: (NSDictionary *)dict{
    [places addObject:dict];
}

+(void)setPrimoPlaces: (NSDictionary *)dict{
    [primoPlaces addObject:dict];
}

+(NSMutableArray *)getPrimoPlaces{ return primoPlaces; }
+(NSMutableDictionary*)getPrimoPhotos{ return primoPhotos; }

+(NSMutableDictionary *)getPhotos{ return photos; }

+(void)setPrimoPhotos: (NSString *)key value:(NSDictionary *)value{
    [primoPhotos setValue:value forKey:key];
}

+(BOOL)getFinished{ return finishedLoadingPlaces; }

+(void)initialize{
    if(!places) places = [[NSMutableArray alloc] init];
    if(!sectionsAndRows) sectionsAndRows = [[NSMutableDictionary alloc] init];
    if(!photos) photos = [[NSMutableDictionary alloc] init];
    primoSectionsAndRows = [[NSMutableDictionary alloc] init];
    primoPlaces = [[NSMutableArray alloc] init];
    primoPhotos = [[NSMutableDictionary alloc] init];
    finishedLoadingPlaces = false;
}

+(void)reloadPlaces{
    RandomPlaces *r = [[RandomPlaces alloc] init];
    primoPlaces = [[NSMutableArray alloc] init];
    primoSectionsAndRows = [[NSMutableDictionary alloc] init];
    primoPhotos = [[NSMutableDictionary alloc] init];
    finishedLoadingPlaces = false;
    [r getPoundDefineNumberOfPlaces];
    while (![RandomPlaces getFinished]) { [NSThread sleepForTimeInterval:0.3]; }
}

-(instancetype)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(void)getPoundDefineNumberOfPlaces{
    self.counter = [[NSNumber alloc] initWithInt:0];
    while ([[RandomPlaces getPrimoPlaces] count] < MAX_PLACES.integerValue) {
        self.counter = [NSNumber numberWithInt:0];
        long goal = MAX_PLACES.integerValue - (int)[[RandomPlaces getPrimoPlaces] count];
        for (int i = 0; i < goal; i++) { [self getPlace]; self.howMany++; }
        while ((int)self.counter.integerValue < goal) { [NSThread sleepForTimeInterval:0.3];}
    }
    NSLog(@"Total API calls: %d", self.howMany);
    [RandomPlaces parseOutSectionsAndRows];
    places = primoPlaces;
    sectionsAndRows = primoSectionsAndRows;
    photos = primoPhotos;
    finishedLoadingPlaces = true;
}

-(void)getPlace{
    NSURLRequest *request = [NSURLRequest requestWithURL:[FlickrFetcher URLforRandomPlace]];
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request completionHandler:^(NSURL *localFile, NSURLResponse *response, NSError *error) {
        if(!error){
            NSDictionary* json = nil;
            if (localFile) {
                json = [NSJSONSerialization
                        JSONObjectWithData:[NSData dataWithContentsOfURL:localFile]
                        options:kNilOptions
                        error:nil];
            }else{
                @synchronized(self.counter){
                    self.counter = [NSNumber numberWithInt:((int)self.counter.integerValue + 1)];
                }
                return;
            }
            NSString *place_id = [json valueForKeyPath:[NSString stringWithFormat:@"%@.%@",FLICKR_RESULTS_PLACES, FLICKR_PLACE_ID]];
            [self getPlaceInfo:place_id dictionary:json]; self.howMany++;
        }
    }];
    [task resume];
}

-(void)getPlaceInfo:(id)place_id dictionary:(NSDictionary *)json{
    NSURLSessionConfiguration *conf = [NSURLSessionConfiguration ephemeralSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:conf];
    NSURLRequest *r = [NSURLRequest requestWithURL:[FlickrFetcher URLforPhotosInPlace:place_id maxResults:MAX_PHOTOS.intValue]];
    NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:r completionHandler:^(NSURL *localFile, NSURLResponse *response, NSError *error) {
        if(!error){
            NSDictionary* dict = nil;
            if (localFile) {
                
                dict = [NSJSONSerialization
                        JSONObjectWithData:[NSData dataWithContentsOfURL:localFile]
                        options:kNilOptions
                        error:nil];
            }
            
            bool valid = false;
            if (dict) {
                if ([[dict objectForKey:@"stat"] isEqualToString:@"ok"]){
                    NSNumber *total = (NSNumber*)[dict valueForKeyPath:FLICKR_PHOTOS_TOTAL];
                    if(total.integerValue > MAX_PHOTOS.integerValue){
                        valid = true;
                    }else{ valid = false; }
                }else{ valid = false; }
            }else{ valid = false; }
            
            if(valid){
                @synchronized(primoPlaces){
                    [RandomPlaces setPrimoPlaces:json];
                }
                @synchronized(primoPhotos){
                    [RandomPlaces setPrimoPhotos:place_id value:dict];
                }
            }
        }
        @synchronized(self.counter){
            self.counter = [NSNumber numberWithInt:((int)self.counter.integerValue + 1)];
        }
    }];
    [task resume];
}

+(void)parseOutSectionsAndRows{
    for (int i = 0; i < [primoPlaces count]; i++) {
        NSDictionary *t = [primoPlaces objectAtIndex:i];
        NSString *location = [t valueForKeyPath:[NSString stringWithFormat:@"%@.%@",FLICKR_RESULTS_PLACES, FLICKR_PLACE_LOCATION]][0];
        NSArray *locs = [location componentsSeparatedByString:@", "];
        NSString *country = [locs objectAtIndex:([locs count] - 1)];
        if (!primoSectionsAndRows[country]) {
            [primoSectionsAndRows setValue:[[NSMutableArray alloc]init] forKey:country];
        }
        NSMutableArray *temp = [primoSectionsAndRows objectForKey:country];
        [temp addObject:locs];
        [primoSectionsAndRows setValue:temp forKey:country];
    }
}

+(NSDictionary *)getSectionsAndRows{
    return sectionsAndRows;
}
@end