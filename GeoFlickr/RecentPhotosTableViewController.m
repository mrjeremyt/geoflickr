//
//  RecentPhotosTableViewController.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "RecentPhotosTableViewController.h"
#import "RecentPhotos.h"
#import "ViewController.h"

@interface RecentPhotosTableViewController () 
@property (nonatomic, strong) NSArray *recents;
@end

@implementation RecentPhotosTableViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.recents = [RecentPhotos getRecentPhotos];
    self.tabBarController.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStyleBordered target:self action:@selector(doThing)];
    
    
}

-(void)doThing{
    [RecentPhotos clearAll];
    [self.tableView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{ self.tabBarController.title = @"Recent Photos"; [self.tableView reloadData]; }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView { return 1; }

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    int count = (int)[self.recents count];
    count = (count > 20) ? 20 : count;
    return count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RecentPhotoCell" forIndexPath:indexPath];
    NSDictionary *dict = [self.recents objectAtIndex:(int)([self.recents count] - 1 - indexPath.row)];
    cell.textLabel.text = dict[@"title"];
    cell.detailTextLabel.text = dict[@"description"];
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    ViewController *vc = (ViewController *)[[segue destinationViewController] topViewController];
    NSDictionary *dict = [self.recents objectAtIndex:(int)([self.recents count] - 1 - indexPath.row)];
    vc.textTitle = dict[@"title"];
    vc.textDescription = dict[@"description"];
    vc.imageURL = dict[@"imageURL"];
}
@end