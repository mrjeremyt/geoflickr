//
//  RootTableViewController.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/4/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "RootTableViewController.h"
#import "RandomPlaces.h"
#import "FlickrFetcher.h"
#import "PhotosTableViewController.h"
#import "RecentPhotos.h"

@interface RootTableViewController ()
@property (nonatomic) NSMutableArray *places;
@end

@implementation RootTableViewController

- (IBAction)refresh:(id)sender {
    [self.refreshControl beginRefreshing];
    
    dispatch_queue_t otherQ = dispatch_queue_create("Q", NULL);
    dispatch_async(otherQ, ^{
        [RandomPlaces reloadPlaces];
        [self.tableView reloadData];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.refreshControl endRefreshing];
        });
    });
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tabBarController.title = @"Random Places";
    self.places = [RandomPlaces getPlaces];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *array = [[defaults objectForKey:@"recents"] mutableCopy];
    if (array) {
        NSMutableArray *result = [[NSMutableArray alloc] init];
        for (int i = 0; i < [array count]; i++) {
            NSMutableDictionary *dict = [array[i] mutableCopy];
            NSString *title = dict[@"title"];
            NSString *description = dict[@"description"];
            NSString *imageUrl = dict[@"imageURL"];
            NSURL *imageURL = [NSURL URLWithString:imageUrl];
            [dict removeAllObjects];
            [dict setObject:title forKey:@"title"];
            [dict setObject:description forKey:@"description"];
            [dict setObject:imageURL forKey:@"imageURL"];
            [result addObject:dict];
        }
        NSLog(@"are we in here?");
        [RecentPhotos setRecentPhotos:result];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    self.tabBarController.title = @"Random Places";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSArray *t = [[RandomPlaces  getSectionsAndRows] allKeys];
    return [t count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *t = [[RandomPlaces  getSectionsAndRows] allKeys];
    t = [t sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return [[[RandomPlaces getSectionsAndRows] valueForKey:t[section]] count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    NSArray *t = [[RandomPlaces  getSectionsAndRows] allKeys];
    t = [t sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    return t[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"RandomPlace" forIndexPath:indexPath];
    NSArray *t = [[RandomPlaces  getSectionsAndRows] allKeys];
    t = [t sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *title = t[indexPath.section];
    NSArray *rows = [[RandomPlaces getSectionsAndRows] valueForKey:title];
    NSMutableArray *tempTitles = [[NSMutableArray alloc] init];
    for (int i = 0; i < [rows count]; i++) {
        NSArray *temp = [rows objectAtIndex:i]; [tempTitles addObject:temp[0]];
    }
    NSArray *sortedTitles = [tempTitles sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *subtitle = sortedTitles[indexPath.row];
    for (int i = 0; i < [rows count]; i++) {
        if ([[rows objectAtIndex:i][0] isEqualToString:subtitle]) {
            cell.textLabel.text = subtitle;
            cell.detailTextLabel.text = [rows objectAtIndex:i][1];
            break;
        }
    }
    return cell;
}


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSArray *t = [[RandomPlaces  getSectionsAndRows] allKeys];
    t = [t sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *title = t[indexPath.section];
    NSArray *rows = [[RandomPlaces getSectionsAndRows] valueForKey:title];
    NSMutableArray *tempTitles = [[NSMutableArray alloc] init];
    for (int i = 0; i < [rows count]; i++) {
        NSArray *temp = [rows objectAtIndex:i];
        [tempTitles addObject:temp[0]];
    }
    NSArray *sortedTitles = [tempTitles sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    NSString *subtitle = sortedTitles[indexPath.row];
    NSArray *checkAgainst = nil;
    for (int i = 0; i < [rows count]; i++) {
        if ([[rows objectAtIndex:i][0] isEqualToString:subtitle]) {
            checkAgainst = [rows objectAtIndex:i]; break;
        }
    }
    NSArray *places = [RandomPlaces getPlaces];
    NSString *place_id = nil;
    for (int i = 0; i < [places count]; i++) {
        NSDictionary *t = [places objectAtIndex:i];
        NSString *location = [t valueForKeyPath:[NSString stringWithFormat:@"%@.%@",FLICKR_RESULTS_PLACES, FLICKR_PLACE_LOCATION]][0];
        NSArray *locs = [location componentsSeparatedByString:@", "];
        if ([locs isEqualToArray:checkAgainst]) {
            place_id = [t valueForKeyPath:[NSString stringWithFormat:@"%@.%@",FLICKR_RESULTS_PLACES, FLICKR_PLACE_ID]][0]; break;
        }
    }
    NSDictionary *primoPhotos = [RandomPlaces getPhotos];
    NSArray *keySet = [primoPhotos allKeys];
    NSDictionary *photos = nil;
    for (int i = 0; i < [keySet count]; i++) {
        if ([keySet[i][0] isEqualToString:place_id]) {
            photos = [primoPhotos objectForKey:keySet[i]];
            break;
        }
    }
    PhotosTableViewController *p = [segue destinationViewController];
    p.photos = [photos valueForKeyPath:FLICKR_RESULTS_PHOTOS];
    [p setTitle:subtitle];
}
@end