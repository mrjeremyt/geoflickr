//
//  RecentPlaces.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "RecentPhotos.h"

@implementation RecentPhotos

+(NSMutableArray *)getRecentPhotos{ return recentPhotos; }

+(void)addToRecentPhotos:(NSDictionary *)dict{
    if (![recentPhotos containsObject:dict]) {
        [recentPhotos addObject:dict];
    }
    if ([recentPhotos count] >= 50) { [RecentPhotos cleanUpArray]; }
}

+(void)setRecentPhotos:(NSMutableArray *)photos{ recentPhotos = photos; }

+(void)clearAll{
    [recentPhotos removeAllObjects];
}

+(void)initialize{
    if(!recentPhotos) recentPhotos = [[NSMutableArray alloc] init];
}

+(void)cleanUpArray{
    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:recentPhotos];
    NSRange range = NSRangeFromString([NSString stringWithFormat:@"0 %d", (int)([recentPhotos count] - 20)]);
    [temp removeObjectsInRange:range];
    [RecentPhotos setRecentPhotos:temp];
}



@end
