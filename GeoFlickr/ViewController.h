//
//  ViewController.h
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/3/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *titleTextView;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (nonatomic) NSString *textTitle;
@property (nonatomic) NSString *textDescription;

@property (nonatomic, strong) NSDictionary *photo;
@property (nonatomic, strong) NSURL *imageURL;

@end

