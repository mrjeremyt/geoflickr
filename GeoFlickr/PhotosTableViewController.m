//
//  PhotosTableViewController.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/10/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "PhotosTableViewController.h"
#import "FlickrFetcher.h"
#import "ViewController.h"
#import "RecentPhotos.h"

@interface PhotosTableViewController ()

@end

@implementation PhotosTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.photos count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PhotoInfo" forIndexPath:indexPath];
    NSDictionary *photo = [self.photos objectAtIndex:indexPath.row];
    
    NSString *title = photo[FLICKR_PHOTO_TITLE];
    NSDictionary *description = photo[@"description"];
    NSString *d = description[@"_content"];
    if ([title length]) {
        cell.textLabel.text = title;
        if ([d length]) { cell.detailTextLabel.text = d; }
        else{ cell.detailTextLabel.text = @"Unknown"; }
    }else{
        if ([d length]) { cell.textLabel.text = d; }
        else{ cell.textLabel.text = @"Unknown"; }
        cell.detailTextLabel.text = @"Unknown";
    }
    return cell;
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    NSDictionary *photo = [self.photos objectAtIndex:indexPath.row];
    ViewController *vc = (ViewController *)[[segue destinationViewController] topViewController];
    
    NSString *title = photo[FLICKR_PHOTO_TITLE];
    NSDictionary *description = photo[@"description"];
    NSString *d = description[@"_content"];
    if ([title length]) {
        title = title;
        if ([d length]) { d = d; }
        else{ d = @"Unknown"; }
    }else{
        if ([d length]) { title = d; }
        else{ title = @"Unknown"; }
        d = @"Unknown";
    }
    
    vc.textTitle = title;
    vc.textDescription = d;
    vc.imageURL = [FlickrFetcher URLforPhoto:photo format:FlickrPhotoFormatLarge];
    NSMutableDictionary *photoDetails = [[NSMutableDictionary alloc] init];
    [photoDetails setValue:title forKey:@"title"];
    [photoDetails setValue:d forKey:@"description"];
    [photoDetails setValue:vc.imageURL forKey:@"imageURL"];
    [RecentPhotos addToRecentPhotos:photoDetails];
}

@end
