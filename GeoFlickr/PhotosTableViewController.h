//
//  PhotosTableViewController.h
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/10/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotosTableViewController : UITableViewController
@property (nonatomic, strong) NSArray *photos;
@property (nonatomic) id place_id;
@end
