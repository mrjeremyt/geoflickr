//
//  AppDelegate.h
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/3/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

