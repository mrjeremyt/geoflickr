//
//  RecentPhotos.h
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/11/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSMutableArray *recentPhotos = nil;

@interface RecentPhotos : NSObject

+(NSArray *)getRecentPhotos;
+(void)addToRecentPhotos: (NSDictionary *)dict;
+(void)setRecentPhotos: (NSMutableArray *)photos;
+(void)clearAll;



@end
