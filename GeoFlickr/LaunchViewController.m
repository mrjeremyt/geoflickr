//
//  LaunchViewController.m
//  GeoFlickr
//
//  Created by Jeremy Thompson on 3/8/15.
//  Copyright (c) 2015 Jeremy Thompson. All rights reserved.
//

#import "LaunchViewController.h"
#import "RandomPlaces.h"

#define LOADING_0 @"Loading data "
#define LOADING_1 @"Loading data ."
#define LOADING_2 @"Loading data .."
#define LOADING_3 @"Loading data ..."

@interface LaunchViewController ()
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic) NSTimer *t;
@property (nonatomic) int counter;
@property (nonatomic) int titleIndex;
@end

@implementation LaunchViewController
-(void)initPlaces{
    RandomPlaces *r = [[RandomPlaces alloc] init];
    dispatch_async(dispatch_queue_create("Q", NULL), ^{
        [r getPoundDefineNumberOfPlaces];
    });
    
    while ([RandomPlaces getFinished] != true) {
        self.titleIndex++;
        dispatch_async(dispatch_queue_create("U", NULL), ^{
            if (self.titleIndex >= 4) { self.titleIndex = 0; }
            if (self.titleIndex == 0) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.title = LOADING_0;
                    [self.navigationController reloadInputViews];
                });
            }else if (self.titleIndex == 1) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.title = LOADING_1;
                });
            }else if (self.titleIndex == 2) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.title = LOADING_2;
                });
            }else if (self.titleIndex == 3) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.navigationItem.title = LOADING_3;
                });
            }
        });
        [NSThread sleepForTimeInterval:0.3];
    }
    [self.navigationController performSegueWithIdentifier:@"LaunchDone" sender:self];
}

-(void)viewDidAppear:(BOOL)animated{
    [self initPlaces];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = LOADING_3;
    self.counter = 0;
    self.titleIndex = 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
